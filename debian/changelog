libws-commons-util-java (1.0.1-10) unstable; urgency=medium

  * Team upload.
  * Removed Niels Thykier from the uploaders (Closes: #770582)
  * Build with Maven instead of Ant
  * Moved the package to Git
  * Standards-Version updated to 4.1.4
  * Switch to debhelper level 11
  * Fixed the watch file

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 20 Apr 2018 17:51:39 +0200

libws-commons-util-java (1.0.1-9) unstable; urgency=low

  * Team upload.
  * Upload to unstable.

 -- tony mancill <tmancill@debian.org>  Wed, 15 May 2013 15:49:14 -0700

libws-commons-util-java (1.0.1-8) experimental; urgency=low

  [ Jakub Adam ]
  * Don't specify Bundle-RequiredExecutionEnvironment in manifest.
    - Allows library to import javax.xml.* packages provided by JRE
      when used as OSGi bundle.
  * Bump Standards-Version to 3.9.4: no changes needed.
  * Remove Michael Koch from Uploaders list (Closes: #654099).
    - Thanks for your contribution to this package.
  * Add Jakub Adam to Uploaders list.

  [ Niels Thykier ]
  * Use the canonical SVN URL in the Vcs-Svn field.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Tue, 05 Feb 2013 15:06:05 +0100

libws-commons-util-java (1.0.1-7) unstable; urgency=low

  * Team upload.
  * Install maven metadata:
    - d/control: Build-Depends on maven-repo-helper.
    - d/rules: Use mh_installpoms and mh_installjar to install JAR.
  * d/control: Bump Standards-Version to 3.9.2: no changes needed.
  * d/build.xml: Force source/target javac to 1.4.

 -- Damien Raude-Morvan <drazzib@debian.org>  Mon, 12 Dec 2011 23:21:19 +0100

libws-commons-util-java (1.0.1-6) unstable; urgency=low

  * Team upload.
  * Rename source package to libws-commons-util-java to be consistent with
    other packages.
  * Update Vcs headers.
  * Remove Java runtime from Depends.
  * Remove Vladimír Lapáček from Uploaders list.
  * Update Standards-Version: 3.9.1.

 -- Torsten Werner <twerner@debian.org>  Wed, 31 Aug 2011 14:42:27 +0200

libws-commons-util (1.0.1-5) unstable; urgency=low

  * Added OSGi metadata to the jar file.
  * Removed reference to cdbs's simple patch system in debian/rules.
  * Bumped Standards-Version to 3.8.4 - no changes required.

 -- Niels Thykier <niels@thykier.net>  Sat, 06 Feb 2010 16:39:40 +0100

libws-commons-util (1.0.1-4) unstable; urgency=low

  * Replaced java-gcj-compat with default-jdk as Build-Depends.
  * Added missing ${misc:Depends}.
  * Bumped debhelper compat to 7.
  * Bumped Standards-Version to 3.8.3.
    - Changed section to java.
    - Referred to the Apache 2.0 license rather than quoting it.
    - Removed the "XS-" prefix from the Vcs-* fields.
  * Added myself to uploaders.
  * Converted to 3.0 (quilt) source.

 -- Niels Thykier <niels@thykier.net>  Sat, 21 Nov 2009 14:06:30 +0100

libws-commons-util (1.0.1-3) unstable; urgency=low

  * Set priority of the package from extra to optional.
  * Added Homepage, XS-Vcs-Svn and XS-Vcs-Browser fields to debian/control.
  * Added watch file.

 -- Michael Koch <konqueror@gmx.de>  Sat, 22 Sep 2007 07:44:39 +0200

libws-commons-util (1.0.1-2) unstable; urgency=low

  * Added missing Build-Depends-Indep on ant. Closes: 433745.
  * Added myself to Uploaders.

 -- Michael Koch <konqueror@gmx.de>  Thu, 19 Jul 2007 09:24:30 -0100

libws-commons-util (1.0.1-1) unstable; urgency=low

  * Initial release (Closes: #429570)

 -- Vladimír Lapáček <vladimir.lapacek@gmail.com>  Fri, 27 Apr 2007 22:48:48 +0200
